
V tomto projektu se nastavuji PHP Code Sniffy a definice pro docker image webgarden/cs, ktery se shipuje na docker HUB.

Jednotlivé standardy ze složky standard se nakopírují do vnitřku vendor složky containeru, díky čemuž jsou standardy k dispozici přímo v nabídce phpcs a IDE PhpStormu při používání.

## Pro upgrade definic

Stačí udělat úpravy v souborech. Pro úpravu obrazu ve složce `image` pro úpravu standardu ve složce `standard` a spustit nový build pomocí skriptu `build.sh`, údaje k docker Hubu uveďte do souboru `config.sh`, pod proměnýma `DOCKER_HUB_LOGIN` a `DOCKER_HUB_PASSWORD`.
