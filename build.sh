#!/usr/bin/env bash

DOCKER_HUB_LOGIN=0
DOCKER_HUB_PASSWORD=0

set -e

if [ -e config.sh ]; then
	source config.sh
fi

docker build . --pull --tag webgarden/cs
docker build . --tag webgarden/cbf --file phpcbf.Dockerfile

if [ $DOCKER_HUB_LOGIN = 0 ] || [ $DOCKER_HUB_PASSWORD = 0 ]; then
	echo "Missing credentials in config.sh. Ship skipped."
else
	docker login -u $DOCKER_HUB_LOGIN -p $DOCKER_HUB_PASSWORD
	docker push webgarden/cs
	docker push webgarden/cbf
	docker logout
fi


echo "----------------------------------"
echo "Build complete"


