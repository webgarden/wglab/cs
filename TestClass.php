<?php

namespace Webgarden\CI;

/**
 * @package Webgarden\CI
 */
class ValidClass
{
	
	/**
	 * @var string
	 */
	public $publicProperty;
	
	/**
	 * @var ValidClass
	 */
	private $parent;
	
	/**
	 * @param ValidClass $parent Parent valid class
	 */
	public function __construct(ValidClass $parent)
	{
		$this->parent = $parent;
	}
	
	/**
	 * @return ValidClass
	 */
	public function getParent()
	{
		return $this->parent;
	}
	
	/**
	 * @return int
	 */
	protected function run()
	{
		$something = 0;
		
		// $something == 0 is forbidden
		if ($something === 0) {
			return true;
		}
		
		$x = null;
		
		if (!empty($x)) {
		
		}
		
		return 1;
	}
}
